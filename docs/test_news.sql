CREATE SCHEMA IF NOT EXISTS `test_news` CHARACTER SET utf8 COLLATE utf8_general_ci;

USE `test_news`;

--
-- Структура таблицы `news`
--

CREATE TABLE IF NOT EXISTS `news` (
	`news_id` int(11) NOT NULL,
	`date` DATE,
	`theme_id` int(11) NOT NULL,
	`text` text,
	`title` varchar(255) NOT NULL DEFAULT 'Default news title',
	PRIMARY KEY (`news_id`)
) ENGINE=InnoDB;

--
-- Структура таблицы `themes`
--

CREATE TABLE IF NOT EXISTS `themes` (
  `theme_id` int(11) NOT NULL,
  `theme_title` varchar(255) NOT NULL,
  PRIMARY KEY (`theme_id`)
) ENGINE=InnoDB;

--
-- Дамп данных таблицы `themes`
--

INSERT INTO `themes` (`theme_id`, `theme_title`) VALUES
(1, 'Наука'),
(2, 'Спорт'),
(3, 'Интернет'),
(4, 'Авто'),
(5, 'Глямур'),
(6, 'Искусство');


ALTER TABLE `news` DROP PRIMARY KEY;
ALTER TABLE `news` CHANGE `news_id` `news_id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY;

ALTER TABLE `themes` DROP PRIMARY KEY;
ALTER TABLE `themes` CHANGE `theme_id` `theme_id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY;

--
-- Ограничения внешнего ключа таблицы `news`
--

ALTER TABLE `news` ADD CONSTRAINT `news_ibfk_1` FOREIGN KEY (`theme_id`) REFERENCES `themes` (`theme_id`) ON DELETE CASCADE ON UPDATE CASCADE;
