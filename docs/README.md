### Установка необходимого ПО (Ubuntu Wily x32)###

```
#!Bash

# Обновление списка пакетов
sudo apt-get update
# Установка
sudo apt-get -y install htop apache2 libapache2-mod-php5 \
  php5-{mysql,json,memcache} libzend-framework-php memcached git

echo "mysql-server-5.6 mysql-server/root_password password superpassword" | debconf-set-selections
echo "mysql-server-5.6 mysql-server/root_password_again password superpassword" | debconf-set-selections

sudo apt-get install -y mysql-{client,server}
# Включение модулей и перезапуск индейца
sudo a2enmode rewrite && sudo service apache2 restart

cd /var/www && git clone https://bitbucket.org/thunderpick/amd_test.git amd
```

### Конфигурация виртуального хоста (/etc/apache2/sites-enabled/000-default.conf) ###


```
#!config

<VirtualHost *:80>
  DocumentRoot "/var/www/amd/public"
  ServerName localhost
  ServerAdmin ololo@trololo.net

  <IfModule php5_module>
    # Это в продакшн не пускать или вместо development указать production
    SetEnv APPLICATION_ENV development
  
    <IfModule dir_module>
      DirectoryIndex index.php index.html index.htm
    </IfModule>

    <Directory "/var/www/amd/public">
      Options Indexes MultiViews FollowSymLinks
      AllowOverride All
      Order allow,deny
      Allow from all
      # apache >= 2.2
      Require all granted
    </Directory>

  </IfModule>

  ErrorLog ${APACHE_LOG_DIR}/amd-error.log
  CustomLog ${APACHE_LOG_DIR}/amd-access.log combined

</VirtualHost>
```

### Создание БД и таблиц test_news (пароль - superpassword) ###

```
#!Bash

cat /var/www/amd/docs/test_news.sql | mysql -uroot -p
```

