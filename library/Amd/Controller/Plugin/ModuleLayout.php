<?php
class Amd_Controller_Plugin_ModuleLayout extends Zend_Controller_Plugin_Abstract
{
	public  function preDispatch( Zend_Controller_Request_Abstract $request )
	{
		$bootstrap	= Zend_Controller_Front::getInstance()->getParam('bootstrap');
		$layout 	= $bootstrap->bootstrap('layout')->getResource('layout');
		$moduleName = ($request->getModuleName() != 'default') 
					? $request->getModuleName()
					: false;
		$layoutFile = sprintf('%s%s.%s', 
			$layout->getViewScriptPath(),
			$moduleName,
			$layout->getViewSuffix()
		);

		if($moduleName && is_readable($layoutFile)) 
			$layout->setLayout($moduleName);
	}
}