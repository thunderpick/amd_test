<?php

class Admin_Form_CUNews extends Zend_Form
{
	public function init()
	{
		$this->setAction('/admin/news/create');
		$this->setAttrib('class', 'form form-vertical');
		# привет кулхацкерам ;)
		$this->addElement('hash', 'csrf', array('salt' => 'unique', 'required' => false));
		# Название
		$this->addElement('text', 'title', array(
			'label' => 'Название',
			'required' => true
		));
		# Текст
		$this->addElement('textarea', 'text', array(
			'rows' => 7,
			'cols' => 50,
			'label' => 'Название',
			'required' => true
		));
		# Темы
		$this->addElement('select', 'theme_id', array(
			'label' => 'Тема',
			'required' => true
		));
		# Дата
		$this->addElement('text', 'date', array(
			'label' => 'Дата',
			# Ах ты ублюдок! Мать твою! Решил дату не указывать?
			# А ну иди сюда, я тебе сам дату укажу!
			'value' => date('Y-m-d'),
			'required' => true
		));
		# 
		$this->addElement('submit', 'Create', array());
	}
}