<?php

class Admin_Form_DNews extends Zend_Form
{
	public function init()
	{
		$this->setAction('/admin/news/delete');
		// $this->addElement('hash', 'csrf', array('salt' => 'unique', 'required' => false));

		$this->addElement('hidden', 'news_id', array());
		
		$this->addElement('submit', 'Delete', array());
	}
}