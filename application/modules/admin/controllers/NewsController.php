<?php

class Admin_NewsController extends Zend_Controller_Action
{

    protected $_dbAdapter = null;

    public function init()
    {
        $this->_dbAdapter = $this->getInvokeArg('bootstrap')->getResource('db');
        $this->_dbAdapter->setFetchMode(Zend_Db::FETCH_OBJ);
        $this->_createSidebar();
    }

    public function indexAction()
    {
        $listSql = 'SELECT n.*, t.theme_title AS theme FROM news n '
        		 . 'JOIN themes t ON t.theme_id = n.theme_id '
        		 . '%s ' # WHERE
        		 . 'ORDER BY n.date DESC'; # Начиная с последней
    	
    	$where = '';

    	if($year = $this->_getParam('year', false)) {
    		$where .= $this->_dbAdapter->quoteInto(' (YEAR(n.date) = ?)', $year);
    		if($month = $this->_getParam('month', false)) {
    			$where .= $this->_dbAdapter->quoteInto(' AND (MONTH(n.date) = ?)', $month);
    		}
    	}

    	if($theme = $this->_getParam('theme', false)) {
    		$whereStr = !empty($where) ? ' AND (n.theme_id = ?)' : ' n.theme_id = ?';
    		$where .= $this->_dbAdapter->quoteInto($whereStr, $theme);
    	}

    	$where = !empty($where) ? 'WHERE ' . $where : ' ';

        # Вот так делать опасно - список может быть огромным...
        # Лучше использовать Zend_Db_Select, но он под запретом
        $founded_items = count($items = $this->_dbAdapter->fetchAll(sprintf($listSql, $where)));
	 	$pager = Zend_Paginator::factory($items);
	 	# Текущая страница
	 	$pager->setCurrentPageNumber($this->_getParam('page', 1));
	 	# По 5 на страницу
	 	$pager->setItemCountPerPage(5);
        $this->view->object_list = $pager;
        $this->view->founded_items = $founded_items;
    }

    public function createAction()
    {
        $form = new Admin_Form_CUNews();
        $themesSql = 'SELECT theme_id, theme_title FROM themes';

        # http://framework.zend.com/manual/1.12/ru/zend.db.adapter.html#zend.db.adapter.select.fetchpairs
        $themes = $this->_dbAdapter->fetchPairs($themesSql);
        $form->getElement('theme_id')->setMultiOptions($themes);
        if($this->_request->isPost()) {
        	if($form->isValid($this->_request->getPost())) {
        		$data = $form->getValidValues($this->_request->getPost());
	        	unset($data['csrf']);
	        	$this->_dbAdapter->beginTransaction();
	        	try {
	        		$this->_dbAdapter->insert('news', $data);
	        		$id = $this->_dbAdapter->lastInsertId();
	        		$new = $this->_dbAdapter->fetchRow('SELECT * FROM news WHERE news_id = ?', $id);
	        		$this->_dbAdapter->commit();
	        		$this->_helper->getHelper('Redirector')->gotoUrlAndExit(sprintf('/admin/news/update/id/%d', $id));
	        		exit;
	        	} catch (Exception $e) {
	        		$this->_dbAdapter->rollBack();
	        		$this->_helper->getHelper('Redirector')->gotoUrlAndExit($this->view->url());
	        	}
        	} else {
        		$this->_helper->getHelper('Redirector')->gotoUrlAndExit($this->view->url());
	        }
        }
        $this->view->createForm = $form;
    }

    public function updateAction()
    {
        $form = new Admin_Form_CUNews();
        $form->setAction($this->view->url());

        $form->getElement('Create')->setLabel('Update');

        $themesSql = 'SELECT theme_id, theme_title FROM themes';
        $themes = $this->_dbAdapter->fetchPairs($themesSql);
        $form->getElement('theme_id')->setMultiOptions($themes);

        $existingItemSql = 'SELECT * FROM news WHERE news_id = ?';
        $existingItem = $this->_dbAdapter->fetchRow($existingItemSql, $this->_getParam('id'));
        $form->populate((array)$existingItem);

        if($this->_request->isPost()) {
        	if($form->isValid($this->_request->getPost())) {
        		$data = $form->getValidValues($this->_request->getPost());
	        	unset($data['csrf']);
        		$this->_dbAdapter->beginTransaction();
        		try {
        			$this->_dbAdapter->update(
        				'news', 
        				$data, 
        				$this->_dbAdapter->quoteInto('news_id = ?', $this->_getParam('id'))
    				);
    				
    				$this->_dbAdapter->commit();
    				# Все ОК, повторный сабмит не нужен
    				$this->_helper->getHelper('Redirector')->gotoUrlAndExit($this->view->url());
        		} catch (Exception $e) {
        			$this->_dbAdapter->rollBack();
        			# Ошибка транзакции
        			$this->_helper->getHelper('Redirector')->gotoUrlAndExit($this->view->url());
        		}
        	} else {
        		# Данные испорчены
        		$this->_helper->getHelper('Redirector')->gotoUrlAndExit($this->view->url());
	        }
        }

        $this->view->createForm = $form;
        $deletionForm = new Admin_Form_DNews();
        $deletionForm->getElement('news_id')->setValue($this->_getParam('id'));
        $this->view->deletionForm = $deletionForm;
    }

    protected function _createSidebar()
    {
    	# Годы, месяцы и кол-во
    	$sql = 'SELECT nm.`date`, YEAR(nm.`date`) as y, MONTH(nm.`date`) as m, '
    		 . 'COUNT(nm.news_id) as cnt '
    		 . 'FROM news nm GROUP BY YEAR(nm.`date`), MONTH(nm.`date`)';

    	$rs = $this->_dbAdapter->fetchAll($sql);
    	$result = array();
    	$months = array(
    		1 => 'Январь', 2 => 'Февраль', 3 => 'Март', 4 => 'Апрель',
    		5 => 'Май', 6 => 'Июнь', 7 => 'Июль', 8 => 'Август', 
    		9 => 'Сентябрь', 10 => 'Октябрь', 11 => 'Ноябрь', 12 => 'Декабрь');

    	foreach ($rs as $value)
    		$result[$value->y][$value->m] = array('lc_name' => $months[$value->m], 'count' => $value->cnt);

    	# Темы
    	$sql = 'SELECT t.*, count(n.news_id) AS cnt '
    		 . 'FROM themes t LEFT JOIN news n ON n.theme_id = t.theme_id '
    		 . 'GROUP BY t.theme_id';

    	$themes = $this->_dbAdapter->fetchAll($sql);

    	$this->view->placeholder('sidebar')->set(
    		$this->view->partial(
				'partials/sidebar.phtml', 
				array(
					'data' => $result,
					'themes' => $themes
				)
			)
		);
    }

    public function deleteAction()
    {
    	if(!$this->_request->isPost())
    		$this->_helper->getHelper('Redirector')->gotoUrlAndExit('/admin/news');

    	$deletionForm = new Admin_Form_DNews();
        $deletionForm->getElement('news_id')->setValue($this->_getParam('id'));

        $validator = new Zend_Validate_Db_RecordExists(
		    array(
		        'table' => 'news',
		        'field' => 'news_id',
		        'schema' => 'test_news',
		        'adapter' => $this->_dbAdapter
		    )
		);
        $deletionForm->getElement('news_id')->addValidator($validator);

        if($deletionForm->isValid($this->_request->getParams())) {
        	$data = $deletionForm->getValidValues($this->_request->getParams());
        	$this->_dbAdapter->delete('news', $this->_dbAdapter->quoteInto('news_id = ?', $data['news_id']));
        	$this->_helper->getHelper('Redirector')->gotoUrlAndExit('/admin/news');
        }
        
    }


}







